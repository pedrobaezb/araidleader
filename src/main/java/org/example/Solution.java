package org.example;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Solution {
    private final Logger log = LoggerFactory.getLogger(Solution.class);

    public int[] solution(int K, int M, int[] A) {
        ArrayList<Integer> leaders= new ArrayList<Integer>();

        int[] totals=new int[M+2];
        int leader=(A.length/2)+1;

        Arrays.stream(A).boxed().forEach((item)-> {
            totals[item]++;
        });

        leaders.addAll(
            IntStream.range(0, totals.length)
                .filter(i -> totals[i]>=leader)
                .boxed()
                .collect(Collectors.toList())
        );

        for(int start=0;start<(A.length-(K-1));start++) {
            int end=(start+K);
            int[] delta=Arrays.copyOf(totals,totals.length);

            IntStream.range(start, end).forEach((i)->{
                delta[A[i]]--; delta[A[i]+1]++;
            });

            leaders.addAll(
                    IntStream.range(0, delta.length)
                            .filter(i -> delta[i]>=leader)
                            .boxed()
                            .collect(Collectors.toList())
            );
        }

        return leaders.stream().mapToInt(i -> i).distinct().sorted().toArray();
    }
}
