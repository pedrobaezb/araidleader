package org.example.test;

import org.example.Solution;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

public class SolutionTest {
    private final Logger log = LoggerFactory.getLogger(SolutionTest.class);
    private final Solution solution = new Solution();

    @Test
    void test() {
        int[] leaders=solution.solution(3,5, new int[]{2, 1, 3, 1, 2, 2, 3});
        assertTrue(Arrays.equals(leaders, new int[]{2,3}));
    }

    @Test
    void test2() {
        int[] leaders=solution.solution(4,2, new int[]{1, 2, 2, 1, 2});
        assertTrue(Arrays.equals(leaders, new int[]{2,3}));
    }

    @Test
    void test3() {
        int[] leaders=solution.solution(4,9, new int[]{5, 5, 9, 5, 8, 3, 4, 5, 6, 5, 5, 5});
        assertTrue(Arrays.equals(leaders, new int[]{5}));
    }
}
